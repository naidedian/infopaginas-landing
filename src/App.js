import React from "react";
import Intro from "./components/Intro";
import Guide from "./components/Guide";
import DetailedProducts from "./components/DetailedProducts";

const App = props => {
  return (
    <div>
      <Intro className="pt-4 pb-4" />
      <Guide />
      <DetailedProducts />
    </div>
  );
};

export default App;
