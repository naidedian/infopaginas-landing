import React from "react";
import { Container, Row, Col } from "reactstrap";
import styled from "styled-components";
import magnifierImg from "../images/icon-actions.svg";
import dataImg from "../images/icon-data.svg";
import strategyImg from "../images/icon-strategy.svg";
import Underline from "./Underline";

const Title = styled.h2`
  font-family: "Domine";
`;

const StyledImg = styled.img`
  max-width: 100px;
`;

const Guide = props => {
  return (
    <Container className="text-center mt-6 mb-6 pt-6">
      <Title>Con esta guía aprenderás a:</Title>
      <Underline center={true} color="#3148D5" className="mt-3 mb-5" />
      <Row xs="3">
        <Col xs="12" md="4" className="mt-3 mb-3">
          <StyledImg src={dataImg} width="100" />
          <p className="text-center">Optimizar redes sociales</p>
        </Col>
        <Col xs="12" md="4" className="mt-3 mb-3">
          <StyledImg src={magnifierImg} width="100" />
          <p className="text-center">
            Crear acciones offline con impacto online
          </p>
        </Col>
        <Col xs="12" md="4" className="mt-3 mb-3">
          <StyledImg src={strategyImg} width="100" />
          <p className="text-center">
            Conocer la importancia de los directorios, blogs e influencers
          </p>
        </Col>
      </Row>
    </Container>
  );
};

export default Guide;
