import React from "react";
import styled from "styled-components";
import { Container, Row, Col } from "reactstrap";
import Underline from "./Underline";
import Form from "./Form";
import bookImg from "../images/book-mockup.png";

const MainContainer = styled.div`
  width: 100%;
  height: 100vh;
  background-image: linear-gradient(90deg, #8139ee 30%, #5541e0);
  border-bottom-left-radius: 50%;
  border-bottom-right-radius: 50%;
  color: #fff;

  @media (max-width: 1024px) {
    height: auto;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
  }
`;

const Title = styled.h3`
  font-family: "Domine";
`;

const BookImg = styled.img`
  max-width: 450px;
  width: 100%;
`;

const Intro = props => {
  return (
    <MainContainer className={props.className}>
      <Container>
        <Row>
          <Col md="6" sm="12">
            <div className="mt-5">
              <Title className="mb-0">
                Promociona tu negocio sin gastar mucho dinero
              </Title>
              <p className="mt-3">
                Descarga nuestra guía y aprende a promocionar tu negocio de
                manera inteligente
              </p>
              <Underline className="mt-3 mb-3" color="#43d7e6" />
              <p>
                Llena el formulario y recibe tu eBook <strong>GRATIS</strong>
              </p>
            </div>
            <Form />
          </Col>
          <Col md="6" sm="12">
            <BookImg src={bookImg} />
          </Col>
        </Row>
      </Container>
    </MainContainer>
  );
};

export default Intro;
