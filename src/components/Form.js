import React from "react";
import {
  Form as BootstrapForm,
  Input,
  Row,
  Col,
  FormGroup,
  Label,
  Button
} from "reactstrap";
import styled from "styled-components";

const StyledButton = styled(Button)`
  color: #fff;
  border-color: #fff;
  &:hover {
    background-color: transparent;
    border-color: #fff;
  }
`;

const Form = props => {
  return (
    <BootstrapForm>
      <Row className="mb-3">
        <Col>
          <Input type="text" name="firstName" placeholder="Nombre*" />
        </Col>
        <Col>
          <Input type="text" name="lastName" placeholder="Apellido*" />
        </Col>
      </Row>
      <Row className="mb-3">
        <Col>
          <Input type="email" name="email" placeholder="Email*" />
        </Col>
        <Col>
          <Input type="text" name="phoneNumber" placeholder="Teléfono*" />
        </Col>
      </Row>
      <Row className="mb-3">
        <Col>
          <Input
            type="text"
            name="businessName"
            placeholder="Nombre de Negocio"
          />
        </Col>
      </Row>
      <FormGroup check className="mb-3">
        <Label check>
          <Input type="checkbox" />
          Acepto que me contacten. Su información no se compartirá con terceros.
        </Label>
      </FormGroup>
      <StyledButton outline size="lg">
        Enviar
      </StyledButton>
    </BootstrapForm>
  );
};

export default Form;
