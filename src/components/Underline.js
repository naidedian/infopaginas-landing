import React from "react";
import styled from "styled-components";

const StyledUnderline = styled.div`
  width: 130px;
  height: 6px;
  margin: ${props => (props.center ? "auto" : "0")}
  background-color: ${props => props.color || "#000"};
`;

const Underline = props => {
  return (
    <StyledUnderline
      className={props.className}
      center={props.center}
      color={props.color}
    />
  );
};

export default Underline;
