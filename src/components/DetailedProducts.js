import React from "react";
import { Container, Row, Col, Button } from "reactstrap";
import styled from "styled-components";
import Underline from "./Underline";

// Images
import magnifierImg from "../images/producto-figure-ip.jpg";
import premiazosImg from "../images/producto-figure-premiazos.jpg";
import ujuImg from "../images/producto-figure-uju.jpg";
import fbAdsImg from "../images/producto-figure-fbads.jpg";
import gAdsImg from "../images/producto-figure-gads.jpg";
import webDevImg from "../images/producto-figure-web.jpg";
import ipLogo from "../images/logo-ip.jpg";
import fbAdsLogo from "../images/logo-fbads.jpg";
import gAdsLogo from "../images/logo-gads.jpg";
import ujuLogo from "../images/logo-uju.jpg";
import premiazosLogo from "../images/logo-premiazos.jpg";

const ItemTitle = styled.h2`
  font-family: "Domine";
`;

const StyledImg = styled.img`
  max-width: 500px;

  @media (max-width: 640px) {
    max-width: 300px;
  }
`;

const DetailedProducts = props => {
  return (
    <Container className="mt-5 pt-5">
      <ItemTitle className="text-center">Conoce nuestros productos:</ItemTitle>
      <Underline center={true} color="#3148D5" className="mt-3 mb-6" />
      <Row className="align-items-center mt-4 mb-4">
        <Col className="text-center">
          <StyledImg src={magnifierImg} />
        </Col>
        <Col>
          <img src={ipLogo} />
          <p className="pt-2 pb-2">
            Directorio digital local donde millones de personas buscan los
            mejores negocios en Puerto Rico.
          </p>
          <Button color="primary">Conoce más</Button>
        </Col>
      </Row>
      <Row className="align-items-center flex-wrap-reverse mt-4 mb-4">
        <Col>
          <img src={premiazosLogo} />
          <p className="pt-2 pb-2">
            Recompensa a tus clientes mediante un Loyalty App y aumenta tus
            ventas.
          </p>
          <Button color="primary">Conoce más</Button>
        </Col>
        <Col className="text-center">
          <StyledImg src={premiazosImg} />
        </Col>
      </Row>
      <Row className="align-items-center mt-4 mb-4">
        <Col className="text-center">
          <StyledImg src={ujuImg} />
        </Col>
        <Col>
          <img src={ujuLogo} />
          <p className="pt-2 pb-2">
            El 70% de tus clientes nunca regresan, asegúrate que los tuyos
            vuelvan. Utiliza nuestra tecnología para envíos de mensajes de texto
            y con tan solo unos clicks estarás listo para enviar tus campañas.
          </p>
          <Button color="primary">Conoce más</Button>
        </Col>
      </Row>
      <Row className="align-items-center flex-wrap-reverse mt-4 mb-4">
        <Col>
          <img src={fbAdsLogo} />
          <p className="pt-2 pb-2">
            Aumenta tus clientes y ventas alcanzando a personas interesadas en
            tu negocio.
          </p>
          <Button color="primary">Conoce más</Button>
        </Col>
        <Col className="text-center">
          <StyledImg src={fbAdsImg} />
        </Col>
      </Row>
      <Row className="align-items-center mt-4 mb-4">
        <Col className="text-center">
          <StyledImg src={gAdsImg} />
        </Col>
        <Col>
          <img src={gAdsLogo} />
          <p className="pt-2 pb-2">
            Pautamos tus anuncios donde la gente busca y se informa.
          </p>
          <Button color="primary">Conoce más</Button>
        </Col>
      </Row>
      <Row className="align-items-center flex-wrap-reverse mt-4 mb-4">
        <Col>
          <ItemTitle>Websites</ItemTitle>
          <p className="pt-2 pb-2">
            Desarrollamos websites de calidad que generan tráfico y
            oportunidades de venta para tu negocio.
          </p>
          <Button color="primary">Conoce más</Button>
        </Col>
        <Col className="text-center">
          <StyledImg src={webDevImg} />
        </Col>
      </Row>
    </Container>
  );
};

export default DetailedProducts;
